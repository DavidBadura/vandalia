help:                                                                           ## shows this help
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_\-\.]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

provision:                                                              ## provision whoami
	kubectl create ns vandalia || true
	helm upgrade --force --install vandalia . --namespace vandalia
